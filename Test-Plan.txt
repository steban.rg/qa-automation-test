Regression Test Plan
    Verify that we can Filter Computers By Name
    Verify we can edit computers from the list
    Verify we can add new computers to the database
    Verify that we can remove computers from the database
    Verify that we can update computers in the database
    Verify that we can do the sorting by "Computer name."
    Verify that we can do the sorting by "Introduce."
    Verify that we can do the sorting by "Discontinued."
    Verify that we can do the sorting by "Company."
    Verify that pagination numbers are correct
    Verify that pagination navigation is correct
 
How will this plan be maintained?
    After completing a release, we should do test maintenance by collecting the test writing during the sprint and selecting the ones that we want to include in our regression suite (this will be an agreement between QA, Dev, and PO, based on Severity, or the release notes, to have a good test coverage)
 
How will this be integrated with the pipeline?
    First, we must select a small subset to validate newly built environments.
    This small set could be considered a smoke test (the minimum required to validate an environment as "ready to test")
 
    Second, all test automated have to run during the end of the sprint as a standard regression.
    Automation will take care of the integrity and preserve existent functionality.
    Manual testing will take of new features and last bug fixes and customer issues with high priority.
 
    Third, we can select one more subset of tests to run after the deployment to production is completed.
    This group must represent all the happy paths the customer takes in his regular flow.
    This test is meant to prevent unexpected complaints due to a deployment problem because of a wrong branch or DB failures and reverts/rolls back on time to the previous stable build.