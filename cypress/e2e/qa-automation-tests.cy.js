describe('QA Automation Tests', () => {
  beforeEach(() => {
    cy.visit('https://computer-database.gatling.io/computers')
  })
  it('update Commodore 64 - negative test case to ensure a failure validation displays.', () => {
    cy.goToItsDefinition('Commodore 64')
    cy.get('#name').clear()
    cy.contains('Save this computer').click()
    cy.get('.error').should('contain', 'Failed to refine type : Predicate isEmpty() did not fail.')
  })
  it('Update Commodore 64 - positive test case to ensure a success validation displays.', () => {
    cy.goToItsDefinition('Commodore 64')
    cy.contains('Save this computer').click()
    cy.get('.alert-message').should('contain', 'Computer Commodore 64 has been updated')
  })
  it('Filter computer list by “HP” and create a map of the returned data.', () => {
    cy.get('#searchbox').type('HP')
    cy.get('#searchsubmit').click()
    cy.get('.computers.zebra-striped').find('tr').then((rows) => {
      const computers = []
      rows.each((index, row) => {
        const computer = {}
        const columns = Cypress.$(row).find('td')
        computer.name = Cypress.$(columns[0]).text()
        computer.introduced = Cypress.$(columns[1]).text()
        computer.discontinued = Cypress.$(columns[2]).text()
        computer.company = Cypress.$(columns[3]).text()
        computers.push(computer)
      })
      cy.log(computers)
    })
  })
  it( 'Filter computer list by “IBM” and return a list of computer names on the LAST page of the results.', () => {
    cy.get('#searchbox').type('IBM')
    cy.get('#searchsubmit').click()
    cy.contains('Next').click()
    cy.contains('Next').click()
    cy.get('.computers.zebra-striped').find('tr').then((rows) => {
      const computers = []
      rows.each((index, row) => {
        const computer = {}
        const columns = Cypress.$(row).find('td')
        computer.name = Cypress.$(columns[0]).text()        
        computers.push(computer)
      })
      cy.log(computers)
    })
  })
  it('create a new computer with the name ‘Evans & Sutherland’', () => {
    cy.contains('Add a new computer').click()
    cy.get('#name').type('Evans & Sutherland')
    cy.contains('Create this computer').click()
    cy.get('.alert-message').should('contain', 'Computer Evans & Sutherland has been created')
  })
})